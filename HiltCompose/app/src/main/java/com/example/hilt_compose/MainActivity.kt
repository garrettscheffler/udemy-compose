package com.example.hilt_compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.hilt_compose.ui.theme.HiltComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    //Field Injection
    @Inject
    lateinit var testClass: testClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        println(testClass.testString())
        println(testClass.testString2())
        setContent {
            HiltComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    HiltComposeTheme {
        Greeting("Android")
    }
}

class testClass @Inject constructor(
    //Constructor Injection
    private val testClass2: testClass2
    ) {
    fun testString() = "test String"
    fun testString2() = testClass2.testString2()
}

class testClass2 @Inject constructor() {
    fun testString2() = "test String 2"
}

