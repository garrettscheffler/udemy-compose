package com.example.profilecard

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.profilecard.ui.theme.ProfileCardTheme
import com.example.profilecard.ui.theme.lightGreen

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ProfileCardTheme {
                UsersApplication()
            }
        }
    }
}

@Composable
fun UsersApplication() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "user_list") {
        composable("user_list") {
            UsersListScreen(profiles, navController)
        }
        composable(
            route = "user_details/{user_id}",
            arguments = listOf(navArgument("user_id") {
                type = NavType.IntType
            })) {
                UserProfileDetailsScreen(
                    profile = profiles[it.arguments!!.getInt("user_id")],
                    navController)
            }
    }
}

@Composable
fun UsersListScreen(profiles: List<UserProfile>, navController: NavController?) {
    Scaffold(
        topBar = { AppBar(title = "Users List", icon = Icons.Default.Home) }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            LazyColumn {
                items(profiles) { profile ->
                    ProfileCard(profile = profile, navController!!) {
                        navController.navigate(route = "user_details/${profile.id}")
                    }
                }
            }
        }
    }
}

@Composable
fun AppBar(title: String, icon: ImageVector, iconClick: () -> Unit = {}) {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = icon,
                contentDescription = "Navigation Icon",
                modifier = Modifier.padding(12.dp).clickable { iconClick.invoke() }
            )
        },
        title = { Text(text = title) }
    )
}

@Composable
fun ProfileCard(profile: UserProfile, navController: NavController, onClick: () -> Unit = {}) {
    Card(
        modifier = Modifier
            .padding(top = 8.dp, bottom = 4.dp, start = 16.dp, end = 16.dp)
            .fillMaxWidth()
            .wrapContentHeight(align = Alignment.Top)
            .clickable(onClick = onClick),
        elevation = 8.dp,
        backgroundColor = Color.White
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            ProfilePicture(profile)
            ProfileContent(profile)
        }
    }
}

@Composable
fun ProfilePicture(profile: UserProfile, imageSize: Dp = 72.dp) {
    Card(
        shape = CircleShape,
        border = BorderStroke(
            width = 2.dp,
            color = if (profile.isOnline) MaterialTheme.colors.lightGreen else Color.LightGray
        ),
        modifier = Modifier.padding(16.dp),
        elevation = 4.dp
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(profile.profilePictureURL)
                .crossfade(true)
                .build(),
            contentDescription = "Profile Picture",
            modifier = Modifier.size(imageSize),
            contentScale = ContentScale.Crop,
            placeholder = painterResource(id = R.drawable.profile_picture)
        )
    }
}

@Composable
fun ProfileContent(profile: UserProfile, alignment: Alignment.Horizontal = Alignment.Start) {
    Column(
        modifier = Modifier
            .padding(8.dp),
        horizontalAlignment = alignment
    ) {
        CompositionLocalProvider(LocalContentAlpha provides if (profile.isOnline) 1f else ContentAlpha.medium) {
            Text(text = profile.name, style = MaterialTheme.typography.h5)
        }

        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Text(
                text = if (profile.isOnline) "Active Now" else "Offline",
                style = MaterialTheme.typography.body2
            )
        }
    }
}

@Composable
fun UserProfileDetailsScreen(profile: UserProfile, navController: NavController?) {
    Scaffold(
        topBar = {
            AppBar(title = "User Profile",
                icon = Icons.Default.ArrowBack,
                iconClick = { navController?.popBackStack() }
            )
        }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top
            ) {
                ProfilePicture(profile, imageSize = 240.dp)
                ProfileContent(profile, Alignment.CenterHorizontally)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun UserProfileDetailsPreview() {
    ProfileCardTheme {
        UserProfileDetailsScreen(profiles[0], null)
    }
}

@Preview(showBackground = true)
@Composable
fun UserListPreview() {
    ProfileCardTheme {
        UsersListScreen(profiles = profiles, navController = null)
    }
}