package com.example.profilecard

data class UserProfile(
    val id: Int,
    val name: String,
    val profilePictureURL: String,
    val isOnline: Boolean
)

val profiles =
    listOf(
        UserProfile(id = 0, name = "John Doe",  "https://picsum.photos/300/300",true),
        UserProfile(id = 1, name = "Tom Scott",  "https://picsum.photos/300/300", false),
    )