package com.example.tsaapp.ui.theme

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class MainViewModel: ViewModel() {

    var icon = mutableStateOf<ImageVector?>(null)

    fun onClick() {
        viewModelScope.launch {
            icon.value = getArrow()
            delay(5000)
            icon.value = null
        }
    }

    private fun getArrow(): ImageVector =
        if (Random().nextBoolean()) {
            Icons.Default.ArrowForward
        } else {
            Icons.Default.ArrowBack
        }
}