package com.example.dynamiccomposeexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.dynamiccomposeexample.ui.theme.DynamicComposeExampleTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainScreen()
        }
    }
}

@Composable
fun MainScreen() {
    val greetingListState = remember { mutableStateListOf("John", "Ben", "Eric") }
    val textFieldState = remember { mutableStateOf("") }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        GreetingList(
            greetingListState,
            { greetingListState.add(textFieldState.value) },
            textFieldState.value,
            { textFieldState.value = it })

    }
}

@Composable
fun GreetingList(
        names: List<String>,
        buttonClick: () -> Unit,
        textField: String,
        textFieldListener: (newName: String) -> Unit) {
    for (name in names) {
        Greeting(name = name)
    }

    TextField(value = textField, onValueChange = textFieldListener)

    Button(
        onClick = buttonClick) {
        Text(text = "Add New Name")
    }
}

@Composable
fun Greeting(name: String) {
    Text(
        text = "Hello $name!",
        style = MaterialTheme.typography.h5
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DynamicComposeExampleTheme {
       MainScreen()
    }
}