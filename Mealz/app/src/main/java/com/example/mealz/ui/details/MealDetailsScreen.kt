package com.example.mealz.ui.details

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import coil.compose.AsyncImage
import com.example.mealz.model.response.MealCategory
import java.lang.Float.min


@Composable
fun MealDetailsScreen(meal: MealCategory?) {
    val scrollState = rememberLazyListState()
    val offset = min(1f, 1 - (scrollState.firstVisibleItemScrollOffset / 600f + scrollState.firstVisibleItemIndex))
    val size by animateDpAsState(targetValue = max(100.dp, 200.dp * offset))
//    var profilePictureState by remember { mutableStateOf(MealProfilePictureState.Normal) }
//    val transition = updateTransition(targetState = profilePictureState, label = "")
//    val imageSizeDp by transition.animateDp(label = "") { it.size }
//    val color by transition.animateColor(label = "") { it.color }
//    val borderWidth by transition.animateDp(label = "") { it.borderWidth }

    Surface(color = MaterialTheme.colors.background) {
        Column {
            Surface(elevation = 4.dp) {
                Row(modifier = Modifier.fillMaxWidth()) {
                    Card(
                        modifier = Modifier.padding(16.dp),
                        shape = CircleShape,
                        border = BorderStroke(width = 2.dp, color = Color.Green)
                    ) {
                        AsyncImage(
                            model = meal?.thumbnail,
                            contentDescription = "Thumbnail",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(size)
                                .padding(8.dp)
                        )
                    }
                    Text(
                        text = meal?.name ?: "default name",
                        modifier = Modifier
                            .padding(16.dp)
                            .align(Alignment.CenterVertically)
                    )
                }
            }
            val dummyContentList = (0..100).map { it.toString() }
            LazyColumn(
                state = scrollState,
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                items(dummyContentList) {
                    Text(text = it, modifier = Modifier.padding(24.dp))
                }

            }


//            Button(
//                modifier = Modifier
//                    .padding(16.dp),
//                onClick = {
////                    profilePictureState =
////                        if (profilePictureState == MealProfilePictureState.Normal)
////                            MealProfilePictureState.Expanded
////                        else
////                            MealProfilePictureState.Normal
//                }
//            ) {
//                Text("Change state of meal profile picture")
//            }
        }
    }
}

enum class MealProfilePictureState(val color: Color, val size: Dp, val borderWidth: Dp) {
    Normal(Color.Magenta, 120.dp, 8.dp),
    Expanded(Color.Green, 200.dp, 24.dp)
}