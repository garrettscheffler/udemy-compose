package com.example.mealz.model.response

import com.google.gson.annotations.SerializedName

data class MealCategoriesResponse(val categories: List<MealCategory>)

data class MealCategory(
    @SerializedName("idCategory") val id: String,
    @SerializedName("strCategory") val name: String,
    @SerializedName("strCategoryDescription") val description: String,
    @SerializedName("strCategoryThumb") val thumbnail: String
)