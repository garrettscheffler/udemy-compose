package com.example.mealz.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.mealz.ui.details.MealDetailsScreen
import com.example.mealz.ui.details.MealDetailsViewModel
import com.example.mealz.ui.meals.MealsCategoriesScreen
import com.example.mealz.ui.theme.MealzTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MealzTheme {
               MealzApp()
            }
        }
    }
}

@Composable
private fun MealzApp() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "meals_list" ) {
        composable(route = "meals_list") {
            MealsCategoriesScreen() { mealId ->
                navController.navigate("meal_details/$mealId")
            }
        }
        
        composable(
            route = "meal_details/{mealId}",
            arguments = listOf(navArgument("mealId") {
                type = NavType.StringType
            })
        ) {
            val viewModel: MealDetailsViewModel = viewModel()
           MealDetailsScreen(viewModel.mealState.value)
        }
    }
}