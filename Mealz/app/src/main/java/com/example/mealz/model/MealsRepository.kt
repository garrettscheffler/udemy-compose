package com.example.mealz.model

import com.example.mealz.model.api.MealsWebService
import com.example.mealz.model.response.MealCategoriesResponse
import com.example.mealz.model.response.MealCategory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MealsRepository(private val webService: MealsWebService = MealsWebService()) {

    companion object {
        private var cachedMeals = listOf<MealCategory>()
    }

//    companion object {
//        @Volatile
//        private var instance: MealsRepository? = null
//
//        fun getInstance() = instance ?: synchronized(this) {
//            instance ?: MealsRepository().also { instance = it }
//        }
//
//    }

    suspend fun getMeals(): MealCategoriesResponse {
        val response = webService.getMeals()
        cachedMeals = response.categories
        return response
    }

    fun getMeal(id: String): MealCategory? = cachedMeals.find {
        it.id == id
    }
}