package com.example.mealz.ui.meals

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mealz.model.MealsRepository
import com.example.mealz.model.response.MealCategoriesResponse
import com.example.mealz.model.response.MealCategory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MealsCategoriesViewModel(private val repository: MealsRepository = MealsRepository()): ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            meals.value = getMeals().categories
        }
    }

    val meals = mutableStateOf(listOf<MealCategory>())

    private suspend fun getMeals() = repository.getMeals()

}
