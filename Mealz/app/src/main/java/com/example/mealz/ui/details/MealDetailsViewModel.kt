package com.example.mealz.ui.details

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.mealz.model.MealsRepository
import com.example.mealz.model.response.MealCategory

class MealDetailsViewModel(
        private val savedStateHandle: SavedStateHandle,
    ): ViewModel() {

    private val repository: MealsRepository = MealsRepository()


    var mealState = mutableStateOf<MealCategory?>(null)

    init {
        val mealId = savedStateHandle.get<String>("mealId")?: ""
        mealState.value = repository.getMeal(mealId)
    }
}