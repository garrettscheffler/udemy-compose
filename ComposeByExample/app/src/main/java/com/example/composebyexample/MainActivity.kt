package com.example.composebyexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composebyexample.ui.theme.ComposeByExampleTheme

private const val helloText = "Hello World"

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
           TextStuff(helloText)
        }
    }
}

@Composable
fun TextStuff(displayText: String) {
    Text(
        text = displayText,
        style = TextStyle(
            fontSize = 18.sp,
            fontFamily = FontFamily.Monospace
        )
    )
}

@Composable
fun ListComponent() {
    
}

@Preview(showBackground = true)
@Composable
fun ImageWithTitleSubtitleComponent() {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(16.dp)) {
        Image(painter = painterResource(id = R.drawable.ic_android_black_24dp), contentDescription = "")
        Column(modifier = Modifier.padding(start = 16.dp)) {
            Text(text = "Title")
            Text(text = "Subtitle")
        }
    }
}

@Composable
fun AlertDialogComponent() {
    var showPopup by remember { mutableStateOf(false) }

    Button(onClick = { showPopup = true }) {
        Text(text = "Click Me")
    }

    if (showPopup) {
        AlertDialog(
            onDismissRequest = { showPopup = false },
            text = {
                Text(text = "Congratulations! You just clicked the text successfully")
            },
            confirmButton = {
                Button(
                    onClick = { println("nothing") }
                ) {
                    Text(text = "ok")
                }
            }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeByExampleTheme {
        TextStuff(helloText)
    }
}